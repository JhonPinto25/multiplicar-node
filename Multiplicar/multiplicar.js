const fs = require('fs');
const colors = require('colors');
let data = '';

let crearArchivo = (base, limite) => {

    return new Promise((resolve, reject) => {

        if (!Number(base)) {
            reject(`El valor introducido ${base} no es numero`)
            return;
        }


        for (let index = 1; index <= limite; index++) {

            data += `${base} * ${index} = ${base * index}\n`;

        }

        fs.writeFile(`Tablas/tabla-${base}.txt`, data, (err) => {

            if (err)
                reject(err);
            else
                resolve(`tabla-${base}- al ${limite}.txt`.red)


        });

    });

}

let listarTabla = async(base, limite) => {

    console.log('======================='.green);
    console.log(`====Tabla de ${base}===`.green);
    console.log('======================='.green);

    for (let index = 1; index <= limite; index++) {

        console.log(`${base} * ${index} = ${base * index}\n`);


    }
}


module.exports = {
    crearArchivo,
    listarTabla
}