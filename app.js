const { crearArchivo, listarTabla } = require('./Multiplicar/multiplicar');
const argv = require('./config/yargs').argv;
const colors = require('colors');



// console.log(process.argv);

//let argv2 = process.argv;
// let parametro = argv[2];
// let base = parametro.split('=')[1];

//console.log(argv.base);
//console.log('limite: ', argv.limite);

let comando = argv._[0];

switch (comando) {
    case 'listar':
        listarTabla(argv.base, argv.limite)
            .then(arch => console.log(`Tabla listada del numero ${argv.base} `))
            .catch(err => console.log(err));

        break;

    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`El archivo creado: ${archivo}`))
            .catch(err => console.log(err));

        break;

    default:
        console.log('comando no reconocido');
        break;
}



// console.log(base);